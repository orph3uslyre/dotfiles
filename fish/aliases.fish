#!/usr/bin/env fish

alias -s c="clear"
alias -s qq="exit"
alias -s sai="sudo apt install"
alias -s update-all="sudo apt update && sudo apt upgrade -y"

# requires exa
alias -s l="eza"
alias -s ls="eza"
alias -s ll="eza -la"
alias -s lll="eza -TD"
alias -s cl="clear && echo && eza"
alias -s cll="clear && echo && eza -la"

# ide
if test $(uname) = "Darwin"
    echo "You are running on macOS."
    alias -s rm='rm'
    alias -s nv="/opt/homebrew/bin/nvim"
    alias -s nvim="/opt/homebrew/bin/nvim"
else
    alias -s rm='rm -I --preserve-root'
    alias -s nv="~/.local/bin/nvim-linux64/bin/nvim"
    alias -s nvim="~/.local/bin/nvim-linux64/bin/nvim"
    echo "You are not running on macOS."
end

# saftey nets and confirmation
# do not delete / or prompt if deleting more than 3 files at a time #
if test $(uname) = "Darwin"
    echo "You are running on macOS."
    alias -s rm='rm'
else
    alias -s rm='rm -I --preserve-root'
    echo "You are not running on macOS."
end
alias -s mv='mv -i'
alias -s cp='cp -i'
alias -s ln='ln -i'

# Parenting changing perms on / #
alias -s chown='chown --preserve-root'
alias -s chmod='chmod --preserve-root'
alias -s chgrp='chgrp --preserve-root'

# handy short cuts
alias -s h='history'
alias -s j='jobs -l'
alias -s mkdir='mkdir -pv'
# print simply
alias -s batt='bat -pP'

# git
alias -s gis='git status'
alias -s gib='git branch'
alias -s gic='git commit -m'
alias -s gia='git add'
alias -s gpsh='git push'
alias -s gpll='git pull'
alias -s gid='git diff'
alias -s gich='git checkout'
alias -s gil='git log --oneline'
alias -s gitback='git reset --soft HEAD~1 && git restore --staged .'
alias -s git-delete-merged='git branch --merged | grep -v "^\*\\|main" | xargs -n 1 git branch -d'

# docker
alias -s dcu='docker compose up'
alias -s dcub='docker compose up --build'
alias -s dcd='docker compose down'
alias -s dcl='docker compose logs'
alias -s dps='docker ps'
alias -s dpsa='docker ps --all'
alias -s dcps='docker compose ps'

# movement
alias -s cd..='cd ..'
alias -s ..='cd ..'
alias -s ...='cd ../../'
alias -s ....='cd ../../../'
alias -s .4='cd ../../../../'
alias -s .5='cd ../../../../../'

# rust
alias -s ceval='cargo eval -v'

# copy
# Remember to install xclip `sudo apt-get install xclip`
alias -s cop='xclip -selection clipboard'
